package mis.pruebas.demo.servicios;


import mis.pruebas.demo.modelos.Cliente;
import mis.pruebas.demo.servicios.impl.ServicioClienteImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.List;

public class ServicioClienteImplTest {


    ServicioClienteImpl servicioCliente= new ServicioClienteImpl();


    @Test
    public void pruebaPaginacion(){

        Cliente cliente1 = new Cliente();
        cliente1.documento="1";
        servicioCliente.clientesMap.put(cliente1.documento,cliente1);

        Cliente cliente2 = new Cliente();
        cliente2.documento="2";
        servicioCliente.clientesMap.put(cliente2.documento,cliente2);

        Cliente cliente3 = new Cliente();
        cliente3.documento="3";
        servicioCliente.clientesMap.put(cliente3.documento,cliente3);

        Cliente cliente4 = new Cliente();
        cliente4.documento="4";
        servicioCliente.clientesMap.put(cliente4.documento,cliente4);

        Cliente cliente5 = new Cliente();
        cliente5.documento="5";
        servicioCliente.clientesMap.put(cliente5.documento,cliente5);

        Cliente cliente6 = new Cliente();
        cliente6.documento="6";
        servicioCliente.clientesMap.put(cliente6.documento,cliente6);

        Cliente cliente7 = new Cliente();
        cliente7.documento="7";
        servicioCliente.clientesMap.put(cliente7.documento,cliente7);

        Cliente cliente8 = new Cliente();
        cliente8.documento="8";
        servicioCliente.clientesMap.put(cliente8.documento,cliente8);

        Cliente cliente9 = new Cliente();
        cliente9.documento="9";
        servicioCliente.clientesMap.put(cliente9.documento,cliente9);

        Cliente cliente10 = new Cliente();
        cliente10.documento="10";
        servicioCliente.clientesMap.put(cliente10.documento,cliente10);

        Cliente cliente11 = new Cliente();
        cliente11.documento="11";
        servicioCliente.clientesMap.put(cliente11.documento,cliente11);

        int cantidad=10;

        List<Cliente> lista=servicioCliente.listaClientes(0,cantidad);
        lista.forEach(System.out::println);
        assertEquals(10,lista.size());

        List<Cliente> lista1=servicioCliente.listaClientes(1,cantidad);
        lista1.forEach(System.out::println);
        assertEquals(1,lista1.size());

        List<Cliente> lista2=servicioCliente.listaClientes(2,cantidad);
        lista2.forEach(System.out::println);
        assertEquals(0,lista2.size());

        List<Cliente> lista3=servicioCliente.listaClientes(3,cantidad);
        lista3.forEach(System.out::println);
        assertEquals(0,lista3.size());

        List<Cliente> lista4=servicioCliente.listaClientes(4,cantidad);
        lista4.forEach(System.out::println);
        assertEquals(0,lista4.size());
    }
}
