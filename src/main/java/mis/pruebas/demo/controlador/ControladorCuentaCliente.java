package mis.pruebas.demo.controlador;

import mis.pruebas.demo.modelos.Cliente;
import mis.pruebas.demo.modelos.Cuenta;
import mis.pruebas.demo.servicios.ServicioCliente;
import mis.pruebas.demo.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("${apirest.base.cuenta-cliente}")
public class ControladorCuentaCliente {

    @Autowired
    ServicioCliente servicioCliente;
    @Autowired
    ServicioCuenta servicioCuenta;


    public static class DatosEntradaCuenta {
        public String codigoCuenta;
    };

    //http://localhost:9000/api/v1/clientes/12345678/cuentas
    @GetMapping
    public ResponseEntity obtenerCuentascliente(@PathVariable String documento) {

        try {
            final List<String> listaCuentas = servicioCliente.obtenerCuentasCliente(documento);
            return ResponseEntity.ok().body(listaCuentas);
        } catch (Exception ex) {
            return ResponseEntity.notFound().build();
        }

    }

    //http://localhost:9000/api/v1/clientes/12345678/cuentas
    @PostMapping
    public ResponseEntity agregarCuentaCliente(@PathVariable String documento, @RequestBody DatosEntradaCuenta datosEntradaCuenta) {
        try {
            final Cuenta cuentaExistente= this.servicioCuenta.obtenerCuenta(datosEntradaCuenta.codigoCuenta);
            this.servicioCliente.agregarCuentaCliente(documento, cuentaExistente.getNumero());
            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            return ResponseEntity.notFound().build();
        }

    }

    //http://localhost:9000/api/v1/clientes/12345678/cuentas/093840394
    @GetMapping("/{numero}")
    public ResponseEntity<Cuenta> obtenerCuentaCliente(@PathVariable String documento, @PathVariable String numero) {


        try {
            final Cliente cliente = servicioCliente.obtenerCliente(documento);

           if (!cliente.codigosCuentas.contains(numero))
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);

            final Cuenta cuenta = this.servicioCuenta.obtenerCuenta(numero);
            return ResponseEntity.ok(cuenta);

        } catch (Exception ex) {
            return ResponseEntity.notFound().build();
        }
    }

   /* @PutMapping("/{documento}/cuentas/{numero}")
    public ResponseEntity reemplazarCuenta(@PathVariable("documento") String documento, @PathVariable("numero") String numero, @RequestBody Cuenta cuenta) {
        cuenta.setNumero(numero);
        try {
            this.servicioCliente.reemplazarCuentaCliente(documento, cuenta);

            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            return ResponseEntity.notFound().build();
        }
    }

    // PUT http://localhost:9000/api/v1/cuentas/{numero} + DATOS -> emparcharUnaCuenta(numero, DATOS)
    // PUT http://localhost:9000/api/v1/cuentas/12345678 + DATOS -> emparcharUnaCuenta("12345678", DATOS)
    @PatchMapping("/{documento}/cuentas/{numero}")
    public ResponseEntity emparacharUnaCuenta(@PathVariable("documento") String nroDocumento, @PathVariable("numero") String nroCuenta,
                                    @RequestBody Cuenta cuenta) {
        cuenta.setNumero(nroCuenta);
        try {
            this.servicioCliente.actualizarCuentaCliente(nroDocumento, cuenta);
            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            return ResponseEntity.notFound().build();
        }

    } */

    //http://localhost:9000/api/v1/clientes/12345678/cuentas/093840394
    @DeleteMapping("/{numero}")
    public ResponseEntity eliminarCuentaCliente(@PathVariable("documento") String nroDocumento, @PathVariable("numero") String nroCuenta) {

        try {
            final Cliente cliente = this.servicioCliente.obtenerCliente(nroDocumento);
            final Cuenta cuenta = this.servicioCuenta.obtenerCuenta(nroCuenta);
            if(cliente.codigosCuentas.contains(nroCuenta)) {
                cuenta.setEstado("INACTIVA");
                cliente.codigosCuentas.remove(nroCuenta);
            }

        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.noContent().build();
    }

}
