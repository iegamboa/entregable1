package mis.pruebas.demo.servicios;


import mis.pruebas.demo.modelos.Cuenta;

import java.util.List;

public interface ServicioCuenta {

    public List<Cuenta> obtenerCuentas();

    //CREAR
    public void insertarCuentaNueva(Cuenta cuenta);

    //READ
    public Cuenta obtenerCuenta(String numero);

    //UPDATE
    public void guardarCuenta(Cuenta cuenta);
    public void emparcharCuenta(Cuenta parche);

    //DELETE
    public  void borrarCuenta(String numero);


}
