package mis.pruebas.demo.servicios;

import mis.pruebas.demo.modelos.Cliente;

import java.util.List;


public interface ServicioCliente {

    public void crearCliente(Cliente cliente);
    public Cliente obtenerCliente(String documento);
    // Update (solamente modificar,no crear)
    public void guardarCliente(Cliente cliente);
    public void borrarCliente(String documento);
    public List<Cliente> listaClientes(int pagina, int cantidad);
    public void emparcharCliente(Cliente parche) ;


    public void agregarCuentaCliente(String documento, String numeroCuenta);
    public List<String> obtenerCuentasCliente(String documento);



  /*  public Cuenta obtenerCuentaCliente(String documento,String numero);*/
   /* public void eliminarCuentaCliente(String documento,String numero);
    public void reemplazarCuentaCliente(String documento,Cuenta cuenta);
    public void actualizarCuentaCliente(String documento,Cuenta cuenta );*/
}
