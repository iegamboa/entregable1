package mis.pruebas.demo.servicios.impl;

import mis.pruebas.demo.modelos.Cliente;
import mis.pruebas.demo.modelos.Cuenta;
import mis.pruebas.demo.servicios.ServicioCliente;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ServicioClienteImpl implements ServicioCliente {

    public final Map<String, Cliente> clientesMap = new ConcurrentHashMap<>();

    @Override
    public void crearCliente(Cliente cliente) {
        this.clientesMap.putIfAbsent(cliente.documento, cliente);
    }

    @Override
    public Cliente obtenerCliente(String documento) {
        if (!this.clientesMap.containsKey(documento))
            throw new RuntimeException("No existe el cliente [Documento]: " + documento);
        return this.clientesMap.get(documento);
    }

    @Override
    public void guardarCliente(Cliente cliente) {

        if (!this.clientesMap.containsKey(cliente.documento))
            throw new RuntimeException("No existe el cliente [Documento]: " + cliente.documento);

        //Guardamos las cuentas del cliente para que se mantengan
        final List<String> cuentas = obtenerCuentasCliente(cliente.documento);
        cliente.codigosCuentas=cuentas;

        this.clientesMap.replace(cliente.documento, cliente);
    }

    @Override
    public void borrarCliente(String documento) {

        if (!this.clientesMap.containsKey(documento))
            throw new RuntimeException("No existe el cliente [Documento]: " + documento);
        this.clientesMap.remove(documento);
    }

    @Override
    public List<Cliente> listaClientes(int pagina, int cantidad) {
        List<Cliente> clientes = List.copyOf(this.clientesMap.values());
        final int indiceIncial = Math.min(pagina * cantidad, clientes.size());
        final int indiceFinal = Math.min(indiceIncial + (cantidad), clientes.size());
        System.err.println(String.format("======= ServicioClienteImpl.obtenerClientes(pagina=%d, cantidad=%d): indiceInicial=%d, indiceFinal=%d", pagina, cantidad, indiceIncial, indiceFinal));
        return clientes.subList(indiceIncial, indiceFinal);
    }

    @Override
    public void emparcharCliente(Cliente parche) {
        final Cliente existente = this.clientesMap.get(parche.documento);

        if (parche.edad != existente.edad)
            existente.edad = parche.edad;

        if (parche.nombre != null)
            existente.nombre = parche.nombre;

        if (parche.correo != null)
            existente.correo = parche.correo;

        if (parche.direccion != null)
            existente.direccion = parche.direccion;

        if (parche.telefono != null)
            existente.telefono = parche.telefono;

        if (parche.fechaNacimiento != null)
            existente.fechaNacimiento = parche.fechaNacimiento;

        this.clientesMap.replace(existente.documento, existente);
    }

    @Override
    public void agregarCuentaCliente(String documento, String numeroCuenta) {
        final Cliente cliente = this.obtenerCliente(documento);
        cliente.codigosCuentas.add(numeroCuenta);
    }

    @Override
    public List<String> obtenerCuentasCliente(String documento) {
        return this.obtenerCliente(documento).codigosCuentas;
    }

  /*  @Override
    public Cuenta obtenerCuentaCliente(String documento, String numero) {

        final Cliente cliente = this.obtenerCliente(documento);

        if (!cliente.codigosCuentas.contains(numero))
            throw new RuntimeException("No existe cuenta [numero]: " + numero);
        return cuentaEncontrada;
    }*/

   /* @Override
    public void eliminarCuentaCliente(String documento, String cuenta) {
        final Cliente cliente = this.obtenerCliente(documento);
        boolean eliminado = cliente.cuentas.removeIf(c -> c.getNumero().equals(cuenta));
        if (Boolean.FALSE.equals(eliminado))
            throw new RuntimeException("No existe cuenta [numero]: " + cuenta);

        this.clientesMap.replace(cliente.documento, cliente);
    }

    @Override
    public void reemplazarCuentaCliente(String documento, Cuenta cuenta) {
        this.eliminarCuentaCliente(documento, cuenta.getNumero());
        this.agregarCuentaCliente(documento, cuenta);
    }

    @Override
    public void actualizarCuentaCliente(String documento, Cuenta cuenta) {
        final Cuenta cuentaExistente = this.obtenerCuentaCliente(documento, cuenta.getNumero());
        this.emparcharCuenta(cuentaExistente, cuenta);
        reemplazarCuentaCliente(documento, cuentaExistente);
    }*/

    public void emparcharCuenta(Cuenta cuentaExistente, Cuenta parche) {


        if (parche.getMoneda() != cuentaExistente.getMoneda())
            cuentaExistente.setMoneda(parche.getMoneda());

        if (parche.getSaldo() != null)
            cuentaExistente.setSaldo(parche.getSaldo());

        if (parche.getTipo() != null)
            cuentaExistente.setTipo(parche.getTipo());

        if (parche.getEstado() != null)
            cuentaExistente.setEstado(parche.getEstado());

        if (parche.getOficina() != null)
            cuentaExistente.setOficina(parche.getOficina());

    }
}
