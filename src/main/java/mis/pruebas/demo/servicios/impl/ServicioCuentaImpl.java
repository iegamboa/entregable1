package mis.pruebas.demo.servicios.impl;

import mis.pruebas.demo.modelos.Cuenta;
import mis.pruebas.demo.servicios.ServicioCuenta;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ServicioCuentaImpl implements ServicioCuenta {

    public final Map<String, Cuenta> cuentas = new ConcurrentHashMap<String, Cuenta>();


    @Override
    public List<Cuenta> obtenerCuentas() {
        return List.copyOf(this.cuentas.values());
    }

    @Override
    public void insertarCuentaNueva(Cuenta cuenta) {
        this.cuentas.put(cuenta.getNumero(), cuenta);
    }

    @Override
    public Cuenta obtenerCuenta(String numero) {
        if (!this.cuentas.containsKey(numero))
            throw new RuntimeException("No existe la cuenta: " + numero);
        return this.cuentas.get(numero);
    }

    @Override
    public void guardarCuenta(Cuenta cuenta) {

        if (!this.cuentas.containsKey(cuenta.getNumero()))
            throw new RuntimeException("No existe la cuenta: " + cuenta.getNumero());
        this.cuentas.replace(cuenta.getNumero(), cuenta);
    }

    @Override
    public void emparcharCuenta(Cuenta parche) {

        final Cuenta existente = this.obtenerCuenta(parche.getNumero());

        if (parche.getMoneda() != existente.getMoneda())
            existente.setMoneda(parche.getMoneda());

        if (parche.getEstado() != null)
            existente.setEstado(parche.getEstado());

        if (parche.getSaldo() != existente.getSaldo())
            existente.setSaldo(parche.getSaldo());

        if (parche.getOficina() != null)
            existente.setOficina(parche.getOficina());

        if (parche.getTipo() != null)
            existente.setTipo(parche.getTipo());

        this.cuentas.replace(existente.getNumero(), existente);


    }

    @Override
    public void borrarCuenta(String numero) {
        this.cuentas.remove(numero);
    }
}
