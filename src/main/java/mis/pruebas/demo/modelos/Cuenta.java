package mis.pruebas.demo.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cuenta {
    private String numero;
    private String moneda;
    private String saldo;
    private String tipo;
    private String estado;
    private String oficina;
}
