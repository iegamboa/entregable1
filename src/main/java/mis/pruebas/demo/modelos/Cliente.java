package mis.pruebas.demo.modelos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
@ToString
public class Cliente {

    public String documento;
    public String nombre    ;
    public String edad;
    public String fechaNacimiento;
    public String direccion;
    public String telefono;
    public String correo;

    @JsonIgnore
    public List<String> codigosCuentas = new ArrayList<>();


}
